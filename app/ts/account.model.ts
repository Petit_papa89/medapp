export class Appointment{

	public id:number

	public time:string

	public name:string

	public phone:number

	public notes:string
	
	public constructor(id:number, time:number, name:string, phone:number, notes:string) {
		this.time = id
		this.time = time
		this.name = name
		this.phone = phone
		this.notes = notes
	}
}