import {Component} from 'angular2/core';
import {Appointment} from './account.model';

@Component({
    selector: 'my-app',
    templateUrl: 'app/ts/app.component.html',
    styleUrls:['app/ts/app.component.css']
})


export class AppComponent {
	
	private _selected:Array<boolean> = [false];

	private _appointments:Array<Appointment> = [
		{
			id:1,
			time:"08:00 AM",
			name:"Youssouph Djiba",
			phone: 5555555555,
			notes:"The kid is very sick. needs a doctor ASAP."
		}	
	]

	private _nextId = 2

	private createApp(timeEl:any, nameEl:any, phoneEl:any, notesEl:any){
		this._appointments.push(new Appointment(this._nextId, timeEl.value, 
			nameEl.value, phoneEl.value, notesEl.value))
		this._selected.push(false) //default not selected
		this._nextId++

		timeEl.value = ""
		nameEl.value = ""
		phoneEl.value = 0
		notesEl.value =''
	}

	private removeApp(index:number){
		this._appointments.splice(index, 1)
		this._selected.splice(index, 1)
	}

	private select(index:number){
		this._selected[index] = !this._selected[index]
	}
}
